package tech.digitus.digitusandroidtest;

public class Constants {
    // example request url
    // https://api.openweathermap.org/data/2.5/weather?APPID=9286fd86071c32c41b2770336d782fcf&units=metric&q=Tunis,tn
    public static final String BASE_URL = "https://api.openweathermap.org/data/2.5/weather?units=metric";
    public static final String APP_ID = "9286fd86071c32c41b2770336d782fcf";
}
