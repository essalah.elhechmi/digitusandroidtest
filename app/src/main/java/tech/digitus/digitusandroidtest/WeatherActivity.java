package tech.digitus.digitusandroidtest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import tech.digitus.digitusandroidtest.model.Weather;

public class WeatherActivity extends AppCompatActivity {

    public static final String BUNDLE_WEATHER_KEY = "key_weather_bundle";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        Weather mWeather = (Weather) getIntent().getExtras().getSerializable(BUNDLE_WEATHER_KEY);

        // do what you want with weather object
    }
}
