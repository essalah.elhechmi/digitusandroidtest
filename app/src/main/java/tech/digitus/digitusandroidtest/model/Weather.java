package tech.digitus.digitusandroidtest.model;

import java.io.Serializable;

public class Weather implements Serializable {

    private String _city;
    private String _description;
    private String _status;
    private String _temperature;

    protected Weather(String city,
                      String description,
                      String status,
                      String temperature) {
        _city = city;
        _description = description;
        _status = status;
        _temperature = temperature;
    }

    public String getCity() {
        return _city;
    }

    public void setCity(String _city) {
        this._city = _city;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String _description) {
        this._description = _description;
    }

    public String getStatus() {
        return _status;
    }

    public void setStatus(String _status) {
        this._status = _status;
    }

    public String getTemperature() {
        return _temperature;
    }

    public void setTemperature(String _temperature) {
        this._temperature = _temperature;
    }
}
