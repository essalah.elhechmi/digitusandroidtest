package tech.digitus.digitusandroidtest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;

import tech.digitus.digitusandroidtest.model.Weather;

public class MainActivity extends AppCompatActivity {

    private Button btnGetWeather, btnOpenCityList;
    private EditText editCity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }


    private void init(){
        editCity = findViewById(R.id.editText);
        // complete what you want to do
    }


    /**
     * start the weather screen, with the given weather info
     *
     * @param weather
     */
    private void startWeatherScreen(Weather weather) {
        Intent intent = new Intent(this, WeatherActivity.class);
        intent.putExtra(WeatherActivity.BUNDLE_WEATHER_KEY, weather);
        startActivity(intent);
    }
}
